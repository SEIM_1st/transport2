/* ステッピングモーター制御
    file: stepping_motor.ino
*/

/*
    stmotorlf //左前進用ピン
    stmotorlb //左後退用ピン
    stmotorrf //右前進用ピン
    stmotorrb //右後退用ピン

*/

/*
    drive();      //前進
    reverse();    //後退
    turnr();      //右回転
    turnl();      //左回転

*/

void drive(int spd) {

  PORTF |= _BV(4) | _BV(5);
  delayMicroseconds(spd);

  PORTF &= ~(_BV(4) | _BV(5));
  delayMicroseconds(spd);
  /*
    digitalWrite(stmotorlf, HIGH);
    digitalWrite(stmotorrf, HIGH);

    delayMicroseconds(spd);

    digitalWrite(stmotorlf, LOW);
    digitalWrite(stmotorrf, LOW);

    delayMicroseconds(spd);
  */

}

void drivel(int spd) {
  digitalWrite(stmotorlf, HIGH);

  delayMicroseconds(spd);

  digitalWrite(stmotorlf, LOW);

  delayMicroseconds(spd);
}

void driver(int spd) {
  digitalWrite(stmotorrf, HIGH);

  delayMicroseconds(spd);

  digitalWrite(stmotorrf, LOW);

  delayMicroseconds(spd);
}

void reverse(int spd) {
  PORTF |= _BV(3) | _BV(7);
  delayMicroseconds(spd);

  PORTF &= ~(_BV(3) | _BV(7));
  delayMicroseconds(spd);
  /*
    digitalWrite(stmotorlb, HIGH);
    digitalWrite(stmotorrb, HIGH);

    delayMicroseconds(spd);

    digitalWrite(stmotorlb, LOW);
    digitalWrite(stmotorrb, LOW);

    delayMicroseconds(spd);
  */
}

void reversel(int spd) {
  digitalWrite(stmotorlb, HIGH);

  delayMicroseconds(spd);

  digitalWrite(stmotorlb, LOW);

  delayMicroseconds(spd);
}

void reverser(int spd) {
  digitalWrite(stmotorrb, HIGH);

  delayMicroseconds(spd);

  digitalWrite(stmotorrb, LOW);

  delayMicroseconds(spd);
}

void turnr(int spd) {
  PORTF |= _BV(4) | _BV(7);
  delayMicroseconds(spd);

  PORTF &= ~(_BV(4) | _BV(7));
  delayMicroseconds(spd);

  /*
    digitalWrite(stmotorlf, HIGH);
    digitalWrite(stmotorrb, HIGH);

    delayMicroseconds(spd);

    digitalWrite(stmotorlf, LOW);
    digitalWrite(stmotorrb, LOW);

    delayMicroseconds(spd);
  */
}

void turnl(int spd) {
  PORTF |= _BV(3) | _BV(5);
  delayMicroseconds(spd);

  PORTF &= ~(_BV(3) | _BV(5));
  delayMicroseconds(spd);
  /*
    digitalWrite(stmotorlb, HIGH);
    digitalWrite(stmotorrf, HIGH);

    delayMicroseconds(spd);

    digitalWrite(stmotorlb, LOW);
    digitalWrite(stmotorrf, LOW);

    delayMicroseconds(spd);
  */
}

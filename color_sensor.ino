/* カラーセンサ
    flie: color_sensor.ino
*/

/*
  　　colorseetup(); //ピン設定
  　　checkcolor(); //色判定(整数値)

*/

// Arduinoのピン定義
const int doutPin = A10;   // Dout
const int rangePin = A13;  // Range
const int ckPin = A12;     // CK
const int gatePin = A11;   // Gate

void colorsetup() {
  // ピンモードを設定する。doutPinは入力、それ以外は出力。
  pinMode(doutPin, INPUT);
  pinMode(rangePin, OUTPUT);
  pinMode(ckPin, OUTPUT);
  pinMode(gatePin, OUTPUT);
}

int checkcolor() {
  // put your main code here, to run repeatedly:
  int red, green, blue;  // 測定した値を格納するための変数
  int integration = 50;  // 測定時間(ミリ秒)を格納する変数

  digitalWrite(gatePin, LOW);        // GateとCKをLOWにする。
  digitalWrite(ckPin, LOW);
  digitalWrite(rangePin, HIGH);      // RangeをHIGHにする。

  digitalWrite(gatePin, HIGH);       // GateをHIGHにして測定開始。
  delay(integration);                // 測定時間だけ待つ。
  digitalWrite(gatePin, LOW);        // GateをLOWにして測定終了。
  delayMicroseconds(4);              // 4ミリ秒待つ。
  red = shiftIn12(doutPin, ckPin);   // 赤色の値を読む。
  delayMicroseconds(3);              // 3ミリ秒待つ。
  green = shiftIn12(doutPin, ckPin); // 緑色の値を読む。
  delayMicroseconds(3);              // 3ミリ秒待つ。
  blue = shiftIn12(doutPin, ckPin);  // 青色の値を読む。

  int h = rgb2hsv(red, green, blue);

  if (red > 1000 && green > 1000 && blue > 1000) {
    h = -1;
  }

  return h;
}

int rgb2hsv(int r, int g, int b) {
  int max = r > g ? r : g;
  max = max > b ? max : b;
  int min = r < g ? r : g;
  min = min < b ? min : b;
  int h = max - min;
  if (h > 0) {
    if (max == r) {
      h = (g - b) / h;
    } else if (max == g) {
      h = 2 + (b - r) / h;
    } else {
      h = 4 + (r - g) / h;
    }
    h *= 60;
    if (h < 0) {
      h += 360;
    }
  }

  return h;
}

//12ビットの値を読み込む関数(LSBから送信されるデータを想定)
int shiftIn12(int dataPin, int clockPin) {
  int value = 0;

  for (int i = 0; i < 12; i++) {
    digitalWrite(clockPin, HIGH);           // クロックをHIGHにする
    value |= digitalRead(dataPin) << i;     // データピンの値を読み取り、所定のビットを設定する。
    digitalWrite(clockPin, LOW);            // クロックピンをLOWにする。
  }

  return value;
}

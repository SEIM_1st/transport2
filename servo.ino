/* サーボ
   flie: servo.ino
*/

/*
   servosetup(); //初期位置設定
   catchball(); //ボール掴む
   byebye(); //カラーセンサ部から解放
   gateopen(); //最後の抑えを解放

*/

#include <Servo.h>

Servo arm;
Servo lefthand;
Servo righthand;
Servo push;
Servo gate;

void servosetup() {

  arm.attach(4);
  arm.write(100);

  lefthand.attach(6);
  lefthand.write(0);

  righthand.attach(8);
  righthand.write(90);

  push.attach(5);
  push.write(90);

  gate.attach(7);
  gate.write(90);

}

void armorosu() {
  //arm.write(45);
  lefthand.write(40);
  righthand.write(60);
  delay(1000);
  arm.write(3);
  delay(1000);
}

void catchball(){
  lefthand.write(0);
  righthand.write(90);
  delay(1000);
  arm.write(100);
  delay(1000);
  lefthand.write(30);
  righthand.write(60);
  delay(1000);
  lefthand.write(0);
  righthand.write(90);
}

void byebye() {

  push.write(0);
  delay(1000);
}

void gateopen() {

  gate.write(0);
  delay(1000);
}

/* メイン制御プログラム
    file: main.ino

    require:
    color_sensor.ino
    linetrace.ino
    servo.ino
    stepping_motor.ino

*/
#include <MsTimer2.h>

#define HISPEED 500 //高速
#define SPEED 800 //中速
#define LOWSPEED 300000 //低速

#define WHITE 380 //ライントレース用白基準
#define BLACK 500 //ライントレース用黒基準

#define BALLDISTANCE 490 //ボール距離

//ボール色によって動作切替用
#define RED 0
#define YELLOW 120
#define BLUE 240

//chousei用
#define TURN 0 //両輪回転でライントレース
#define DRIVE 1 //片輪回転でライントレース

//加減速用
volatile int accel = 5000; //加速前の値
volatile int deaccel = SPEED; //減速前の値

const int stmotorlb = 57; //左後
const int stmotorlf = 58; //左前
const int stmotorrf = 59; //右後
const int stmotorrb = 61; //右前

const int swl = 11; //スイッチ左
const int swr = 12; //スイッチ右

int i; //for文用カウンタ
int j = 0; //何もなしで前に進んだ分記録用

int lrcount = 0; //左右カウンタ
boolean sonoba = 0; //ボール前線を踏むとき
boolean limit = 0; //奥まで行ったとき

void setup() {
  delay(1000);
  pinMode(stmotorlb, OUTPUT); //左ステッピングモーター後退
  pinMode(stmotorlf, OUTPUT); //左前進
  pinMode(stmotorrf, OUTPUT); //右前進
  pinMode(stmotorrb, OUTPUT); //右後退

  pinMode(swl, INPUT); //スイッチ左
  pinMode(swr, INPUT); //スイッチ右

  servosetup();
  colorsetup();

  //二本目黒になるまで後退
  MsTimer2::set(1, speedup);
  MsTimer2::start();
  int line = 0;
  while (1) {
    reverse(accel);
    if (analogRead(A8) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK && analogRead(A15) > BLACK) {
      ++line;
      if (line == 2) {
        break;
      }
      while (analogRead(A8) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK && analogRead(A15) > BLACK) {
        reverse(accel);
      }
    }
  }
  MsTimer2::stop();
  accel = 5000;

  //回転して自由ボール落とす
  for (i = 0; i < 300; ++i) {
    turnl(SPEED);
  }
  while (digitalRead(swl) == HIGH) {
    reverse(1200);
  }

  //ラインに戻る
  for (i = 0; i < 500; ++i) {
    drive(SPEED);
  }
  while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
    turnl(SPEED);
  }

  //ライントレース開始
  firsttrace(3);

}

void loop() {

  //少し進んで中心を向く
  for (i = 0; i < 200; ++i) {
    drive(SPEED);
  }
  while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
    turnr(SPEED);
  }

  if (j == 0) { //初回または進まなかったとき
    MsTimer2::set(1, speeddown);
    MsTimer2::start();
    for (i = 0; i < 200; ++i) {
      reverse(deaccel);
    }
    MsTimer2::stop();
    deaccel = SPEED;
  } else { //進んだとき
    for (i = 0; i < 200; ++i) {
      drive(SPEED);
      chousei(DRIVE, SPEED);
    }
    for (i = 0; i < j - 200; ++i) {
      drive(SPEED);
      chousei(DRIVE, SPEED);
    }
  }

  int lr = 0; //左、右、左の動作のための  forカウンタ
  int d = 0, r = 0; //ボール取るときの前後進記録用
  int degree = 0, kyori = 0, ballcount = 0; //角、距離、ボールの数用
  int color[3]; //色

  /* 前進してボール探し */
  do {
    for (lr = 0; lr < 2; lr++) {
      switch (lrcount) {
        case 0: //初回、または前回が左回転だった場合
          //右にゆっくり向きながらボール探し
          for (i = 0; i < 300 ; ++i) {
            turnr(LOWSPEED);
            //距離センサに一番近い角度を見つける
            if (analogRead(A0) > kyori) {
              kyori = analogRead(A0);
              degree = i;
            }
          }
          lrcount = 1;
          break;
        case 1: //前回が右回転だった場合
          //左にゆっくり向きながらボール探し
          for (i = 0; i < 300 ; ++i) {
            turnl(LOWSPEED);
            if (analogRead(A0) > kyori) {
              kyori = analogRead(A0);
              degree = i;
            }
          }
          lrcount = 0;
          break;
      }
      if (kyori > 230 ) { //近かった場合
        break;
      } else {
        switch (lrcount) {
          case 0: //左回転だった場合
            for (i = 0; i < 300; ++i) {
              turnr(SPEED);
            }
            break;
          case 1: //右回転だった場合
            for (i = 0; i < 300; ++i) {
              turnl(SPEED);
            }
            break;
        }
        if (lr == 1) { //左右向いてボールがなかった場合
          //少し進む
          for (i = 0; i < 300; ++i) {
            drive(SPEED);
            chousei(TURN, SPEED);
            ++j;

            //全部黒(奥のライン)になったら回転
            if (analogRead(A8) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK && analogRead (A15) > BLACK && j > 0) {
              for (i = 0; i < 320; ++i) {
                turnr(SPEED);
              }
              while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
                turnr(SPEED);
              }

              j = 0; //回転数リセット
              sonoba = 1; //踏むフラグを立てる
              limit = 1; //奥まで行ったフラグ立てる
              break; //ここのfor文を抜ける
            }
          }
          kyori = 0;
          degree = 0;
          lr = -1;
        }

        kyori = 0;
        degree = 0;
      }
    }

    //さっき一番近かった角度にする
    switch (lrcount) {
      case 0: //左でbreakした場合
        //右に回す
        for (i = 0; i < 300 - degree ; ++i) {
          turnr(LOWSPEED);
        }
        break;
      case 1: //右でbreakした場合
        //左に回す
        for (i = 0; i < 300 - degree ; ++i) {
          turnl(LOWSPEED);
        }
        break;
    }

    //近づいてとる
    while (analogRead(A0) < BALLDISTANCE) {
      drive(LOWSPEED);
      ++d; //進んだ分記録
    }
    while (analogRead(A0) >= BALLDISTANCE) {
      reverse(LOWSPEED);
      ++r;//戻った分記録
    }

    for (i = 0; i < 100; ++i) { //少し下がる
      reverse(LOWSPEED);
    }
    armorosu(); //読んで字のごとく
    for (i = 0; i < 100; ++i) {
      drive(LOWSPEED);
    }
    catchball();
    delay(1000);

    color[ballcount++] = checkcolor(); //カラーセンサの値読む

    //戻る
    for (i = 0; i < d; ++i) {
      reverse(SPEED);
    }
    for (i = 0; i < r; ++i) {
      drive(LOWSPEED);
    }

    //正面向く、向いてほしい
    switch (lrcount) {
      case 0: //左でbreakした場合
        //真ん中が黒に乗るまで右回転
        while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
          turnr(SPEED);
        }
        break;
      case 1: //右でbreakした場合
        //左回転
        while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
          turnl(SPEED);
        }
        break;
    }

    //各項目初期化
    d = 0;
    r = 0;
    kyori = 0;
    degree = 0;
    //もし何も乗ってなかった場合
    if (color[ballcount - 1] == -1) {
      --ballcount;
      servosetup();
      continue; //do whileの最初に飛ぶ
    } else { //乗ってた場合
      if (ballcount == 1 || color[ballcount - 2] == color[ballcount - 1]) { //1個目の場合
        byebye();
        servosetup();
        continue;
      }
      break; //ループを抜け次に進む
    }
  } while (1);
  /* ここまで */

  /* 戻り始めるコード(1個目) */
  if (j < 600 && limit == 0) { //近くで奥まで行ってない場合
    for (i = 0; i < j; ++i) {
      reverse(SPEED);
    }

    for (i = 0; i < 320; ++i) {
      turnr(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnr(SPEED);
    }
  } else if (limit == 0) { //遠くで奥まで行ってない場合

    for (i = 0; i < 320; ++i) {
      turnr(SPEED);
    }

    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnr(SPEED);
    }
    sonoba = 1;
  } else { //奥まで行った場合
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
  }
  /* ここまで */

  /* 籠に向かうコード */
  if (color[0] == BLUE) { //1個目青
    if (sonoba == 1) { //奥まで行ってたら
      backtrace(2); //ライン2本
    } else {
      backtrace(1);//ライン1本目までライントレース
    }
    for (i = 0; i < 100; ++i) {
      drive(HISPEED);
    }

    while (analogRead(A8) < WHITE || analogRead(A9) < WHITE || analogRead(A14) < WHITE || analogRead(A15) < WHITE) {
      drive(HISPEED);
    }
    delay(300);
    for (i = 0; i < 600; ++i) {
      turnl(SPEED);
    }
  } else if (color[0] == YELLOW) { //1個目黄
    if (sonoba == 1) {
      backtrace(3);
    } else {
      backtrace(2);
    }
    delay(300);

    for (i = 0; i < 310; ++i) {
      turnl(SPEED);
    }

    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }
  } else if (color[0] == RED) { //1個目赤
    if (sonoba == 1) {
      backtrace(4);
    } else {
      backtrace(3);
    }
    delay(300);

    for (i = 0; i < 300; ++i) {
      turnl(SPEED);
    }

    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }
  }
  /* ここまで */

  /* ボール落とす */
  gateopen();
  servosetup();
  /* ここまで */

  /* 二個目のボール排出 */
  if (color[0] == BLUE && color[ballcount - 1] == YELLOW) { //青→黄
    for (i = 0; i < 500; ++i) {
      drive(HISPEED);
    }
    for (i = 0; i < 150; ++i) {
      turnr(HISPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnr(SPEED);
    }
    backtrace(1);

    delay(300);
    for (i = 0; i < 300; ++i) {
      turnl(SPEED);
    }
    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }

  } else if (color[0] == BLUE && color[ballcount - 1] == RED) { //青→赤
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    for (i = 0; i < 150; ++i) {
      turnr(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnr(SPEED);
    }
    backtrace(2);

    delay(300);
    for (i = 0; i < 300; ++i) {
      turnl(SPEED);
    }
    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }

  } else if (color[0] == YELLOW && color[ballcount - 1] == RED) { //黄→赤
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnr(SPEED);
    }
    backtrace(1);

    delay(300);
    for (i = 0; i < 300; ++i) {
      turnl(SPEED);
    }
    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }
  } else if (color[0] == YELLOW && color[ballcount - 1] == BLUE) { //黄→青
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
    firsttrace(1);
    delay(300);

    //TODO:後退回転数は適時変えてくれ
    for (i = 0; i < 1170; ++i) {
      reverse(SPEED);
    }
  } else if (color[0] == RED && color[ballcount - 1] == BLUE) { //赤→青
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
    firsttrace(2);
    delay(300);

    //TODO:後退回転数は適時変えてくれ
    for (i = 0; i < 1170; ++i) {
      reverse(SPEED);
    }

  } else if (color[0] == RED && color[ballcount - 1] == YELLOW) { //赤→黄
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
    firsttrace(1);
    for (i = 0; i = 500; ++i) {
      drive(HISPEED);
    }
    delay(300);

    for (i = 0; i < 310; ++i) {
      turnl(SPEED);
    }

    while (digitalRead(swl) == HIGH) {
      reverse(1200);
    }
  }
  /* ここまで */

  /* ボール落とす */
  byebye();
  gateopen();
  servosetup();
  /* ここまで */

  /* 戻るコード */
  if (color[ballcount - 1] == BLUE) {
    firsttrace(2);
  } else if (color[ballcount - 1] == YELLOW) {
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
    firsttrace(2);
  } else if (color[ballcount - 1] == RED) {
    for (i = 0; i < 500; ++i) {
      drive(SPEED);
    }
    while (analogRead(A9) < WHITE || analogRead(A14) < WHITE) {
      turnl(SPEED);
    }
    firsttrace(3);
  }
  /* ここまで */

  /* 奥まで探していたらリセット*/
  if (limit == 1) {
    j = 0;
    sonoba = 0;
    limit = 0;
  }
  /* ここまで */
}

//加速関数
int speedup() {
  if (accel > HISPEED) {
    accel -= 10;
  }

  return accel;
}

//減速関数
int speeddown() {
  deaccel += 5;

  return deaccel;
}

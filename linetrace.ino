/* ライントレース
   file:linetrace.ino
*/

/*
   linetrace(int); //初回など横線カウンタ付きライントレース(暫定、場合追加必要有)
   chousei(); //黒線に乗るよう調整

*/

void firsttrace(int count) { //引数でbreakの本数決め
  int line = 0;
  while (1) {
    drive(HISPEED);
    if (analogRead(A8) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK) {
      ++line;
      if (line == count) {
        line = 0;
        break;
      }
      while (analogRead(A8) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK) {
        drive(HISPEED);
      }
    }
    chousei(DRIVE, HISPEED);
  }
}

void backtrace(int count) { //引数でbreakの本数決め
  int line = 0;
  while (1) {
    drive(HISPEED);
    if (analogRead(A15) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK) {
      ++line;
      if (line == count) {
        line = 0;
        break;
      }
      while (analogRead(A15) > BLACK && analogRead(A9) > BLACK && analogRead(A14) > BLACK) {
        drive(HISPEED);
      }
    }
    chousei(DRIVE, HISPEED);
  }
}

void chousei(int mode, int speed) {
  if (analogRead(A9) < WHITE && analogRead(A14) > BLACK) { //左が黒から出たとき
    while (analogRead(A9) < WHITE) {
      if (mode == TURN){
        turnr(speed);
      } else if (mode == DRIVE){
        drivel(speed);
      }
    }
  } else if (analogRead(A14) < WHITE && analogRead(A9) > BLACK) { //右が黒から出たとき
    while (analogRead(A14) < WHITE) {
      if (mode == TURN){
        turnl(speed);
      } else if (mode == DRIVE){
        driver(speed);
      }
    }
  }
}
